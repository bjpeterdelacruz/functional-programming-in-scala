package examples

object Week2 {
  val x = new Rational(1, 3)                      //> x  : examples.Rational = 1/3
  val y = new Rational(5, 7)                      //> y  : examples.Rational = 5/7
  val z = new Rational(3, 2)                      //> z  : examples.Rational = 3/2
  x.numer                                         //> res0: Int = 1
  x.denom                                         //> res1: Int = 3
  x - y - z                                       //> res2: examples.Rational = -79/42
  y + y                                           //> res3: examples.Rational = 10/7
  x < y                                           //> res4: Boolean = true
  x max y                                         //> res5: examples.Rational = 5/7
  new Rational(2)                                 //> res6: examples.Rational = 2/1
}

// Lecture 2.5
class Rational(x: Int, y: Int) {
  require(y != 0, "denominator must be nonzero")

  def this(x: Int) = this(x, 1)

  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
  val numer = x / gcd(x, y)
  val denom = y / gcd(x, y)

  def + (that: Rational) =
    new Rational(
      numer * that.denom + that.numer * denom,
      denom * that.denom)

  def unary_- : Rational = new Rational(-numer, denom)

  def - (that: Rational) = this + -that

  def < (that: Rational) = this.numer * that.denom < that.numer * this.denom

  def max(that: Rational) = if (this < that) that else this

  override def toString = numer + "/" + denom
}