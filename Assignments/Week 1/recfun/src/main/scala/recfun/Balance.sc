package recfun

object Balance {
    def balance(chars: List[Char]): Boolean = {
      def concat(char: Char): String =
        if (char == '(') "("
        else if (char == ')') ")"
        else ""

      def makeString(chars: List[Char]): String =
        if (chars.isEmpty) ""
        else concat(chars.head) + makeString(chars.tail)

      def check(string: String): Boolean = {
        val temp = string.replaceAll("\\(\\)", "")
        if (temp == string) string.isEmpty
        else check(temp)
      }

      check(makeString(chars))
    }                                             //> balance: (chars: List[Char])Boolean
    balance("())(".toList)                        //> res0: Boolean = false
}