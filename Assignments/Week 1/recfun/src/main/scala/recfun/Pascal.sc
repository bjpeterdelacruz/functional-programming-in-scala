package recfun

object Test {
    def pascal(c: Int, r: Int): Int =
      if (c == 0) 1
      else if (r == c) 1
      else pascal(c, r - 1) + pascal(c - 1, r)    //> pascal: (c: Int, r: Int)Int
    pascal(0, 0)                                  //> res0: Int = 1
    pascal(0, 1)                                  //> res1: Int = 1
    pascal(1, 1)                                  //> res2: Int = 1
    pascal(0, 2)                                  //> res3: Int = 1
    pascal(1, 2)                                  //> res4: Int = 2
    pascal(2, 2)                                  //> res5: Int = 1
    pascal(1, 3)                                  //> res6: Int = 3
}