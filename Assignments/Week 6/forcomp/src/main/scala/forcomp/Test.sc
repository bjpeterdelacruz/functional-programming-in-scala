package forcomp

object Test {
  Anagrams.sentenceOccurrences(List("Hello", "hi"))
                                                  //> res0: forcomp.Anagrams.Occurrences = List((e,1), (h,2), (i,1), (l,2), (o,1))

  Anagrams.wordAnagrams("Miho")                   //> res1: List[forcomp.Anagrams.Word] = List()
  Anagrams.sentenceAnagrams(List("I", "love", "you"))
                                                  //> res2: List[forcomp.Anagrams.Sentence] = List(List(Io, Lev, you), List(Io, yo
                                                  //| u, Lev), List(Lev, Io, you), List(Lev, you, Io), List(olive, you), List(you,
                                                  //|  Io, Lev), List(you, Lev, Io), List(you, olive))
}