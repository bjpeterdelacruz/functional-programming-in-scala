package funsets

object Main extends App {
  import FunSets._
  println(forall(x => x > -2000, x => x < 10))
  println(exists(x => x > -2000, x => x < 10))
}
